package com.aigerimbb.android.simpletwitterapp.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aigerimbb.android.simpletwitterapp.R;
import com.aigerimbb.android.simpletwitterapp.adapter.CustomPagerAdapter;

/**
 * Created by Aigerim on 8/31/2017.
 */

public class TweetsFragment extends Fragment implements TabLayout.OnTabSelectedListener  {

    /**
     * Tablayout vars
     */
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        view = inflater.inflate(R.layout.fragment_tweets,container,false);
        setupTabloyut();
        return view ;
    }

    public void setupTabloyut(){
        //init tablayout
        tabLayout=(TabLayout)view.findViewById(R.id.tweets_tablayout);
        // add tabs
        tabLayout.addTab(tabLayout.newTab().setText(view.getContext().getString(R.string.own_tweets)));
        /*tabLayout.addTab(tabLayout.newTab().setText(view.getContext().getString(R.string.all_tweets)));
        */tabLayout.addTab(tabLayout.newTab().setText(view.getContext().getString(R.string.all_tweets)));

        //init view pager
        viewPager=(ViewPager)view.findViewById(R.id.tweets_pager);
        viewPager.setAdapter(new CustomPagerAdapter(getFragmentManager(),tabLayout.getTabCount()));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setOffscreenPageLimit(2);
        tabLayout.addOnTabSelectedListener(this);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
