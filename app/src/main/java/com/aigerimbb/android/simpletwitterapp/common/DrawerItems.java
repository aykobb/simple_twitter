package com.aigerimbb.android.simpletwitterapp.common;

/**
 * Created by Aigerim on 8/31/2017.
 */


public class DrawerItems {
    private String title;
    private int icon;

    public DrawerItems(String title, int icon) {
        this.title = title;
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
