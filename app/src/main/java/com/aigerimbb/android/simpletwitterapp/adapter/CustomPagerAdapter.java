package com.aigerimbb.android.simpletwitterapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.aigerimbb.android.simpletwitterapp.tab.AllTweetsTab;
import com.aigerimbb.android.simpletwitterapp.tab.OwnTweetsTab;
import com.aigerimbb.android.simpletwitterapp.tab.PopularTweetsTab;

/**
 * Created by Aigerim on 8/31/2017.
 */

public class CustomPagerAdapter extends FragmentStatePagerAdapter {

    private int tabCount;
    public CustomPagerAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount=tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new OwnTweetsTab();/*
            case 1:
                return new PopularTweetsTab();*/
            case 1:
                return new AllTweetsTab();
            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return tabCount;
    }

}
