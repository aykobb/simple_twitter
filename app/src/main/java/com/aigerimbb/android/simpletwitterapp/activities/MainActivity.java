package com.aigerimbb.android.simpletwitterapp.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.aigerimbb.android.simpletwitterapp.R;
import com.aigerimbb.android.simpletwitterapp.adapter.DrawerListAdapter;
import com.aigerimbb.android.simpletwitterapp.common.Constants;
import com.aigerimbb.android.simpletwitterapp.common.DrawerItems;
import com.aigerimbb.android.simpletwitterapp.common.LoadingProgressDialog;
import com.aigerimbb.android.simpletwitterapp.fragment.ContactsFragment;
import com.aigerimbb.android.simpletwitterapp.fragment.ProfileFragment;
import com.aigerimbb.android.simpletwitterapp.fragment.TweetsFragment;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.tweetcomposer.ComposerActivity;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    /**
     * Toolbar vars
     */
    private Toolbar toolbar;
    private TextView toolbarTitle;
    private ImageView homeButton;

    /**
     * Drawer vars
     */
    private DrawerLayout drawerLayout;
    private ListView drawerListView;
    private ArrayList<DrawerItems> drawerItems;
    private DrawerListAdapter drawerListAdapter;
    private String [] drawerTitles;
    private TypedArray drawerIcons;
    private TextView userTV, userNameTV, followingTV, followersTV;
    private ImageView closeDrawer, avatar;
    private FloatingActionButton postTweet;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupToolbar();
        setupDrawer(savedInstanceState);
        setupTweetingButton();
    }

    private void setupToolbar(){
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        homeButton = (ImageView)findViewById(R.id.home);
        homeButton.setOnClickListener(this);
        toolbarTitle = (TextView)findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void setupDrawer(Bundle bundle){
        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        drawerListView = (ListView) findViewById(R.id.drawer_list);
        drawerTitles  = getResources().getStringArray(R.array.drawer_titles);
        drawerIcons = getResources().obtainTypedArray(R.array.drawer_icons);
        /*header*/
        View drawerHeader =getLayoutInflater().inflate(R.layout.drawer_header, null,false);
        setupDrawerHewder(drawerHeader);

        drawerListView.addHeaderView(drawerHeader);
        drawerItems = new ArrayList<DrawerItems>();
        for(int i=0; i<drawerTitles.length;i++){
            drawerItems.add(new DrawerItems( drawerTitles[i], drawerIcons.getResourceId(i,-1)));
        }
        drawerIcons.recycle();
        drawerListView.setOnItemClickListener(new SlideMenuClickListener());
        drawerListAdapter = new DrawerListAdapter(this, drawerItems);
        drawerListView.setAdapter(drawerListAdapter);

        if(bundle == null){
            displayView(1);
        }
    }

    private void setupDrawerHewder(View drawerHeader){
        userTV = (TextView)drawerHeader.findViewById(R.id.drawer_header_name);
        userTV.setText(Constants.user.name);
        userNameTV = (TextView)drawerHeader.findViewById(R.id.drawer_header_username);
        userNameTV.setText("@"+Constants.user.screenName);
        followingTV=(TextView)drawerHeader.findViewById(R.id.drawer_header_following);
        followingTV.setText(getString(R.string.followings)+" "+Constants.user.friendsCount);
        followersTV=(TextView)drawerHeader.findViewById(R.id.drawer_header_followers);
        followersTV.setText(getString(R.string.followers)+" "+Constants.user.followersCount);
        avatar=(ImageView)drawerHeader.findViewById(R.id.avatar);
        Picasso.with( this )
                .load(Constants.user.profileImageUrl)
                .error( R.drawable.twitter_icon )
                .placeholder( R.drawable.progress_animation )
                .into(avatar);
        closeDrawer = (ImageView) drawerHeader.findViewById(R.id.drawer_close_iv);
        closeDrawer.setOnClickListener(this);
    }

    private void setupTweetingButton(){
        postTweet =(FloatingActionButton)findViewById(R.id.post_tweet);
        postTweet.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            /*drawer items*/
            case R.id.drawer_close_iv:
                drawerLayout.closeDrawers();
                break;
            /* toolbar items */
            case R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                //mainDL.openDrawer(Gravity.START);
                break;
            case R.id.post_tweet:
                postTweet();
                break;
        }
    }

    private void postTweet(){
        final TwitterSession session = TwitterCore.getInstance().getSessionManager()
                .getActiveSession();
        final Intent intent = new ComposerActivity.Builder(this)
                .session(session)
                .createIntent();
        startActivity(intent);
    }
    /**
     * Slide menu item click listener
     * */
    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // display view for selected nav drawer item
            drawerListView.setItemChecked(position, true);
            drawerListAdapter.notifyDataSetChanged();
            displayView(position);
        }

    }
    /**
     * Diplaying fragment view for selected nav drawer list item
     * */
    private void displayView(int position){
        Fragment fragment=null;
        switch (position){
            case 1:
                fragment = new TweetsFragment();
                break;
            case 2:
                fragment = new ProfileFragment();
                break;
            case 3:
                fragment = new ContactsFragment();
                break;
            case 4:
                finish();
                break;
            default:
                break;

        }
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, fragment,position+"").commit();
            // update selected item and title, then close the drawer
            drawerListView.setItemChecked(position, true);
            drawerListView.setSelection(position);
            toolbarTitle.setText(drawerTitles[--position]);
            //setToolbarTitle(drawerTitles[position]);
            drawerLayout.closeDrawer(drawerListView);
        } else {
            // error in creating fragment
        }
    }
}
