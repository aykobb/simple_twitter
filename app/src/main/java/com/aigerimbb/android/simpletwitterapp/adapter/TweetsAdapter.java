package com.aigerimbb.android.simpletwitterapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import com.aigerimbb.android.simpletwitterapp.R;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.tweetui.CompactTweetView;
import com.twitter.sdk.android.tweetui.TweetView;

import java.util.ArrayList;

/**
 * Created by Aigerim on 8/31/2017.
 */

public class TweetsAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Tweet> tweets;

    public TweetsAdapter(Context context, ArrayList<Tweet>tweets){
        this.context=context;
        this.tweets=tweets;
    }
    @Override
    public int getCount() {
        return tweets.size();
    }

    @Override
    public Object getItem(int position) {
        return tweets.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.tweets_row,null);
        }
        CompactTweetView tweetView = (CompactTweetView)convertView.findViewById(R.id.tweet_view);
        tweetView.setTweet(tweets.get(position));
        return convertView;
    }
}