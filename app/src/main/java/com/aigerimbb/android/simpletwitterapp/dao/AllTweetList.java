package com.aigerimbb.android.simpletwitterapp.dao;

import com.twitter.sdk.android.core.models.Tweet;

/**
 * Created by Aigerim on 9/1/2017.
 */

public class AllTweetList extends CustomList<Tweet> {
    private static AllTweetList tweets =new AllTweetList();
    private AllTweetList(){}

    public static AllTweetList getInstance(){ return tweets;};
}