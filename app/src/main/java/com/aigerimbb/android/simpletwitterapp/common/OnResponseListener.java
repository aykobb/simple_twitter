package com.aigerimbb.android.simpletwitterapp.common;

import com.aigerimbb.android.simpletwitterapp.api.ApiEnum;
import com.twitter.sdk.android.core.Result;

import retrofit2.Response;

/**
 * Created by Aigerim on 9/1/2017.
 */

public interface OnResponseListener {
    void onResponse (Response result, ApiEnum restEnum);
}
