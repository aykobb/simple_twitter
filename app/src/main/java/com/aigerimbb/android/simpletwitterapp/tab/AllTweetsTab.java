package com.aigerimbb.android.simpletwitterapp.tab;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.aigerimbb.android.simpletwitterapp.R;
import com.aigerimbb.android.simpletwitterapp.adapter.TweetsAdapter;
import com.aigerimbb.android.simpletwitterapp.api.ApiEnum;
import com.aigerimbb.android.simpletwitterapp.common.Constants;
import com.aigerimbb.android.simpletwitterapp.common.OnResponseListener;
import com.aigerimbb.android.simpletwitterapp.dao.AllTweetList;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Tweet;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

/**
 * Created by Aigerim on 8/31/2017.
 */

public class AllTweetsTab extends Fragment implements SwipeRefreshLayout.OnRefreshListener, OnResponseListener{
    private View view;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ListView listView;
    private TweetsAdapter tweetsAdapter;
    private ProgressBar footerPB;
    private TextView notFoundTV;
    private boolean flagBottom = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        view = inflater.inflate(R.layout.tab_all_tweets,container,false);
        getHomeTweets();
        notFoundTV=(TextView)view.findViewById(R.id.not_found_tv);
        initView();
        return view;
    }


    private void initView(){
        //footerPB.setVisibility(View.VISIBLE);
        listView = (ListView) view.findViewById(R.id.all_tweets_lv);
        footerPB =(ProgressBar)view.findViewById(R.id.all_tweets_footer_pb);
        swipeRefreshLayout=(SwipeRefreshLayout)view.findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setColorSchemeColors(getContext().getResources().getColor(R.color.colorAccent));
        swipeRefreshLayout.setOnRefreshListener(this);

        tweetsAdapter = new TweetsAdapter(view.getContext(), AllTweetList.getInstance().getList());
        tweetsAdapter.notifyDataSetChanged();
        //gridView
        listView.setAdapter(tweetsAdapter);
        listView.setOnScrollListener(new CustomOnScrollListener());
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                /*Intent intent = new Intent(getContext(),FilmInfoActivity.class);
                intent.putExtra("movieObject",FilmList.getInstance().list.get(position));
                startActivity(intent);*/
            }
        });
    }

    private void getHomeTweets(){
        Constants.apiClient.getStatusesService().homeTimeline(20,null,null,false,true,false,false)
                .enqueue(new Callback<List<Tweet>>() {
                    @Override
                    public void success(Result<List<Tweet>> result) {
                        AllTweetList.getInstance().list .clear();
                        AllTweetList.getInstance().getList().addAll(result.data);
                        tweetsAdapter.notifyDataSetChanged();
                        listView.setVisibility(View.VISIBLE);
                        footerPB.setVisibility(View.GONE);
                    }

                    @Override
                    public void failure(TwitterException exception) {
                        Toast.makeText(getContext(),getString(R.string.not_load),Toast.LENGTH_LONG).show();
                    }
                });
    }


    @Override
    public void onResponse(Response result, ApiEnum restEnum) {

    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }


    class CustomOnScrollListener implements GridView.OnScrollListener{

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem,
                             int visibleItemCount, int totalItemCount) {

            if (firstVisibleItem + visibleItemCount == totalItemCount && flagBottom) {
                flagBottom =false;
            }
        }

        @Override
        public void onScrollStateChanged(AbsListView view,
                                         int scrollState) {
        }
    }
}
