package com.aigerimbb.android.simpletwitterapp.adapter;

/**
 * Created by Aigerim on 8/31/2017.
 */
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aigerimbb.android.simpletwitterapp.R;
import com.aigerimbb.android.simpletwitterapp.common.DrawerItems;

import java.util.ArrayList;

/**
 * Created by Aigerim on 7/19/2017.
 */

public class DrawerListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<DrawerItems> drawerItems;

    public DrawerListAdapter(Context context, ArrayList<DrawerItems> drawerItems){
        this.context=context;
        this.drawerItems =drawerItems;
    }
    @Override
    public int getCount() {
        return drawerItems.size();
    }

    @Override
    public Object getItem(int position) {
        return drawerItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.drawer_row, null);
        }

        /*ImageView icon = (ImageView)convertView.findViewById(R.id.drawer_row_imageview);
        icon.setImageResource(drawerItems.get(position).getIcon());
*/
        TextView title =(TextView) convertView.findViewById(R.id.drawer_row_tv);
        //title.setTypeface();
        title.setText(drawerItems.get(position).getTitle());

        return convertView;
    }
}