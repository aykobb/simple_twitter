package com.aigerimbb.android.simpletwitterapp.dao;

import java.util.ArrayList;

/**
 * Created by Aigerim on 9/1/2017.
 */
public class CustomList<T> {
    public ArrayList<T> list=new ArrayList<T>();
    public String next;
    public String prev;
    public int count;

    public ArrayList<T> getList() {
        return list;
    }

    public void setList(ArrayList<T> list) {
        this.list = list;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getPrev() {
        return prev;
    }

    public void setPrev(String prev) {
        this.prev = prev;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
