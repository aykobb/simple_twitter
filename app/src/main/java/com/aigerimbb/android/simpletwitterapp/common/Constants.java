package com.aigerimbb.android.simpletwitterapp.common;

import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.User;

/**
 * Created by Aigerim on 8/31/2017.
 */

public class Constants {
    public static User user;
    public static TwitterSession session;
    public static TwitterAuthToken authToken;
    public static TwitterApiClient apiClient;
}
