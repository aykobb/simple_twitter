package com.aigerimbb.android.simpletwitterapp.common;

/**
 * Created by Aigerim on 9/1/2017.
 */
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.aigerimbb.android.simpletwitterapp.R;


/**
 * Created by Aigerim on 8/9/2017.
 */

public class LoadingProgressDialog extends ProgressDialog {

    public LoadingProgressDialog(Context context) {
        super(context,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
    }

    public LoadingProgressDialog(Context context, int theme) {
        super(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_progress_dialog);
        ImageView la = (ImageView) findViewById(R.id.animation);
        la.setImageResource(R.drawable.progress_animation);
        final Window window = this.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    public static ProgressDialog getProgressDialog(Context context) {
        LoadingProgressDialog dialog = new LoadingProgressDialog(context);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        return dialog;
    }
}
