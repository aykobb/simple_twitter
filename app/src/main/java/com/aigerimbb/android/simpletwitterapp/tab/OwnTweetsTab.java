package com.aigerimbb.android.simpletwitterapp.tab;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.aigerimbb.android.simpletwitterapp.R;
import com.aigerimbb.android.simpletwitterapp.common.Constants;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.tweetui.TimelineResult;
import com.twitter.sdk.android.tweetui.TweetTimelineListAdapter;
import com.twitter.sdk.android.tweetui.UserTimeline;

/**
 * Created by Aigerim on 9/1/2017.
 */

public class OwnTweetsTab extends ListFragment implements SwipeRefreshLayout.OnRefreshListener{

    private View view;
    private SwipeRefreshLayout swipeRefreshLayout;
    private  TweetTimelineListAdapter adapter;
    private  Handler handler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.timeline, container, false);
        setupList();
        setupViews();
        setupHandler();
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    private void setupViews(){
        swipeRefreshLayout=(SwipeRefreshLayout)view.findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setColorSchemeColors(getContext().getResources().getColor(R.color.colorAccent));
        swipeRefreshLayout.setOnRefreshListener(this);

    }

    private void setupList(){
        final UserTimeline userTimeline = new UserTimeline.Builder()
                .screenName(Constants.user.screenName)
                .build();
        adapter = new TweetTimelineListAdapter.Builder(getActivity())
                .setTimeline(userTimeline)
                .build();
        setListAdapter(adapter);
    }

    private void setupHandler(){
        handler = new Handler();
        handler.postDelayed( new Runnable() {

            @Override
            public void run() {
                refreshList();
                handler.postDelayed( this, 60 * 1000 );
            }
        }, 60 * 1000 );
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        refreshList();
    }

    private void refreshList(){
        adapter.refresh(new Callback<TimelineResult<Tweet>>() {
            @Override
            public void success(Result<TimelineResult<Tweet>> result) {
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void failure(TwitterException exception) {
                Toast.makeText(getContext(),getResources().getString(R.string.not_load), Toast.LENGTH_LONG).show();
            }
        });
    }
}
