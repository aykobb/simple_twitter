package com.aigerimbb.android.simpletwitterapp.api;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;

import com.aigerimbb.android.simpletwitterapp.common.LoadingProgressDialog;
import com.aigerimbb.android.simpletwitterapp.common.OnResponseListener;
import com.twitter.sdk.android.core.Result;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Aigerim on 8/31/2017.
 */

public class CustomAsyncTask extends AsyncTask<Call, Void, Response> {

    private Context ctx;
    private ProgressDialog pDialog;
    public OnResponseListener delegate = null;
    private ApiEnum rest;
    private View view;

    public CustomAsyncTask(Context ctx, View v, OnResponseListener delegate, ApiEnum rest){
        this.ctx = ctx;
        view = v;
        this.rest = rest;
        this.delegate = delegate;
    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(view==null){
            pDialog = LoadingProgressDialog.getProgressDialog(ctx);
            pDialog.show();
        }
        else
            view.setVisibility(View.VISIBLE);
    }

    @Override
    protected Response doInBackground(Call... params) {
        Call call = params[0];
        Response response = null;
        try {
            response = call.execute();
        } catch (IOException e) {
            return null;
        }
        return response;
    }

    @Override
    protected void onPostExecute(Response res) {
        super.onPostExecute(res);
        if(view!=null)
            view.setVisibility(View.GONE);
        else {
            pDialog.dismiss();
            pDialog = null;
        }
        delegate.onResponse(res , rest);
    }
}