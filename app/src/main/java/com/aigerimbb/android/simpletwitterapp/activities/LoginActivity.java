package com.aigerimbb.android.simpletwitterapp.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.aigerimbb.android.simpletwitterapp.R;
import com.aigerimbb.android.simpletwitterapp.api.ApiEnum;
import com.aigerimbb.android.simpletwitterapp.api.CustomAsyncTask;
import com.aigerimbb.android.simpletwitterapp.common.Constants;
import com.aigerimbb.android.simpletwitterapp.common.OnResponseListener;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;

import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements OnResponseListener {
    /*** VIEWS ***/
    private TwitterLoginButton loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initTwitterConfig();
        setFullScreen();
        setContentView(R.layout.activity_login);
        initViews();
    }

    private void initTwitterConfig(){
        TwitterConfig config = new TwitterConfig.Builder(this)
                .logger(new DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(new TwitterAuthConfig(getResources().getString(R.string.com_twitter_sdk_android_CONSUMER_KEY),
                        getResources().getString(R.string.com_twitter_sdk_android_CONSUMER_SECRET)))
                .debug(true)
                .build();
        Twitter.initialize(config);
    }
    private void setFullScreen(){
        // full screen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private  void initViews(){
        loginButton =(TwitterLoginButton) findViewById(R.id.login_button);
        loginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                // Do something with result, which provides a TwitterSession for making API calls
                Constants.session  = TwitterCore.getInstance().getSessionManager().getActiveSession();
                Constants.authToken = Constants.session.getAuthToken();
                Constants.apiClient = TwitterCore.getInstance().getApiClient();
                new CustomAsyncTask(LoginActivity.this ,null,LoginActivity.this, ApiEnum.USER_INFO).execute(Constants.apiClient.getAccountService().verifyCredentials(true,true,true));
            }

            @Override
            public void failure(TwitterException exception) {
                Log.v("NURTV", exception.getMessage());
                Toast.makeText(LoginActivity.this,getString(R.string.not_load),Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        loginButton.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onResponse(Response result, ApiEnum restEnum) {
        if(result!=null){
            Constants.user = (User) result.body();
            Intent intent =new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
        }else {
            Toast.makeText(this,getString(R.string.not_load),Toast.LENGTH_LONG).show();
        }

    }
}
