# Project - *Simple Twitter App* #
** *Simple Twitter App* ** is an android app that allows a user to view own tweets and home timelines, compose and post a new tweet.

**Time spent:** 20 hours spent in total



### Functionality ###

* *Requirred features*
    
    * Pages
        
        * login page 
        
        * user timeline page 
        
        * create tweet page
        
    * Funcionality
    
        * list tweets should be updated automatically each minute
        
        * list tweets should be updated on list pull down

* *Optional features*

     * Home timeline is added
     
     * Drawer navigation added
     
     * User information with data(name, username, follower/ings count, profile picture)   
     
     
     
     
  ### Video Walkthrough ###
  
![Alt text](https://bitbucket.org/aykobb/simple_twitter/downloads/video.gif)    




  ### Open-source libraries used ###
  
  * Picasso
  
  * Twitter SDK